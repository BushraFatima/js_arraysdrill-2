import {got} from './data-1.js';

const countAllPeople = () => {

    const housesList = got.houses;
    const countPeople = housesList.reduce((result, house) => {
        return  result + (house.people).length;

    }, 0);

    return countPeople;

};

console.log(countAllPeople());

const peopleByHouses = () => {

    const housesList = got.houses;
    const peoplePerHouse = housesList.reduce((houseObj, house) => {

        houseObj[house.name] = (house.people).length;
        return houseObj;   

    }, {});

    return peoplePerHouse;

};

console.log(peopleByHouses());

const everyone = () => {

    const housesList = got.houses;

    let peopleNames = housesList.map((house) => {

        let peoples = house.people;
        let names = peoples.map((person) => { return person.name; });

        return names;

    });

    peopleNames = peopleNames.flat();

    return peopleNames;

};

console.log(everyone());

const nameWithS = (namesList) => {

    const sInNames = namesList.filter((names) => { 
        return names.includes('s') || names.includes('S');

    });

    return sInNames;

};

console.log(nameWithS(everyone()));

const nameWithA = (namesList) => {

    const aInNames = namesList.filter((names) => { 
        return names.includes('a') || names.includes('A');

    });

    return aInNames;

};

console.log(nameWithA(everyone()));

const surnameWithS = (namesList) => {

    const surnameS = namesList.filter((names) => {

        let nameList = names.split(' ');
        let surname = nameList[nameList.length -1];

        return surname[0] === 'S';

    });

    return surnameS;
}

console.log(surnameWithS(everyone()));

const surnameWithA = (namesList) => {

    const surnameA = namesList.filter((names) => {

        let nameList = names.split(' ');
        let surname = nameList[nameList.length -1];

        return surname[0] === 'A';

    });

    return surnameA;

}

console.log(surnameWithA(everyone()));

const peopleNameOfAllHouses = () => {

    const houseList = got.houses;
    let nameHousePeoples = houseList.reduce((houseObj, house) => {

        let peoples = house.people;
        houseObj[house.name] = peoples.map((person) => { return person.name; });

        return houseObj;

    }, {});

    return nameHousePeoples;

}

console.log(peopleNameOfAllHouses());


